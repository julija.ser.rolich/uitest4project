import playwright from 'playwright';

let browser;
let context;
let page;

async function goto2(url){
    await page.goto(url);
    return page;
};

async  function run2(){
    browser = await playwright.chromium.launch({
        headless: true,
        //slowMo: 1000,
    });
    context = await browser.newContext();
    page = await context.newPage();

};

async  function stop2(){
    await page.close();
    await browser.close();
};

export  { goto2, run2, stop2 }
