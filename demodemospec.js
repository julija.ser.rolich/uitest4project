//import playwright from 'playwright';
//const playwright = require("playwright");
import {goto, run, stop} from './framework/lib/browser';
import chai from 'chai';
const {expect} = chai;

describe ('Это демо сьюит', () => {
    let page;
    beforeEach(async () => {
        await run();
        page = await goto('https://try.vikunja.io/login');
    });
    afterEach(async () => {
        await stop();
    });

        it('Это демо 1 тест', async () => {
            await page.fill('#username', 'demo');
            await page.click('#password');
            await page.fill('#password', 'demo');
            await page.click('.is-primary');
            await page.waitForNavigation({waitUntil: 'networkidle'});
            const profileName = ('.user > .dropdown > .dropdown-trigger > .button > .username');
            await page.waitForSelector(profileName);
            const profileNameText = await page.textContent(profileName);

            expect('hj').to.have.string(profileNameText);
            }
        )


    }
);

