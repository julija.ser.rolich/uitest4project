import {goto2, run2, stop2} from './framework/lib/newbrowser';
import {url} from './framework/config/url';
import chai from 'chai';
const {expect} = chai;

describe('Первый сьюит: авторизация', () => {
    let page;
    beforeEach(async () => {
        await run2();
        page = await goto2(url.idemobspb);
    });
    afterEach(async () => {
      await stop2();
    });


    it('Первый тест, валидная авторизация', async () => {
        const usernameField = ('.container > #login-container > #login-form > .form-control:nth-child(2) > input');
        await page.waitForSelector(usernameField);
        await page.click(usernameField);
        await page.fill(usernameField, '');
        await page.fill(usernameField, 'demo');
        const passwordField = ('.container > #login-container > #login-form > .form-control:nth-child(3) > input');
        await page.waitForSelector(passwordField);
        await page.click(passwordField);
        await page.fill(passwordField, '');
        await page.fill(passwordField, 'demo');
        const loginButton = ('.container > #login-container > #login-form #login-button');
        await page.waitForSelector(loginButton);
        await page.click(loginButton);
        await page.waitForSelector('body #login-otp-button');
        await page.click('body #login-otp-button');
        const userGreeting = await page.textContent('.content > #controls > #user #user-greeting');

        expect('Hello World!').to.have.string(userGreeting);
           });

    it('Второй тест, запрет авторизации при невалидных данных', async () => {
            const usernameField = ('.container > #login-container > #login-form > .form-control:nth-child(2) > input');
            await page.waitForSelector(usernameField);
            await page.click(usernameField);
            await page.fill(usernameField, '');
            await page.fill(usernameField, 'demo3');
            const passwordField = ('.container > #login-container > #login-form > .form-control:nth-child(3) > input');
            await page.waitForSelector(passwordField);
            await page.click(passwordField);
            await page.fill(passwordField, '');
            await page.fill(passwordField, 'demo4');
            const loginButton = ('.container > #login-container > #login-form #login-button');
            await page.waitForSelector(loginButton);
            await page.click(loginButton);
            const alert = await page.textContent('body > .container > #login-container > #alerts-container > .alert');

            expect(alert).to.match(/Неверные данные/, 'Вау, внимание на авторизацию!!');
    });


    it('Третий тест: на вкладке Платежи и переводы имеется элемент таблицы (есть транзакции)', async () => {
        const usernameField = ('.container > #login-container > #login-form > .form-control:nth-child(2) > input');
        await page.waitForSelector(usernameField);
        await page.click(usernameField);
        await page.fill(usernameField, '');
        await page.fill(usernameField, 'demo');
        const passwordField = ('.container > #login-container > #login-form > .form-control:nth-child(3) > input');
        await page.waitForSelector(passwordField);
        await page.click(passwordField);
        await page.fill(passwordField, '');
        await page.fill(passwordField, 'demo');
        const loginButton = ('.container > #login-container > #login-form #login-button');
        await page.waitForSelector(loginButton);
        await page.click(loginButton);
        await page.waitForSelector('body #login-otp-button');
        await page.click('body #login-otp-button');

        const payments = ('.navbar #payments-form');
        await page.waitForSelector(payments);
        await page.click(payments);
        const history = ('#contentbar > #payments-left-menu > #dashboard-payment-type-menu > li > .history');
        await page.waitForSelector(history);
        await page.click(history);

        const historyFilter = ('#contentbar > .tab-content > #history-form > #payment-history-filter > select');
        await page.waitForSelector(historyFilter);
        await page.click(historyFilter);
        await page.selectOption('#contentbar > .tab-content > #history-form > #payment-history-filter > select', '10028');
        const datePicker = ('.tab-content > #history-form > #payment-history-filter > #from-date > .input-small');
        await page.waitForSelector(datePicker);
        await page.click(datePicker);
        await page.waitForSelector('.datepicker:nth-child(18) > .datepicker-days > .table-condensed > tbody > tr:nth-child(1) > .day:nth-child(2)');
        await page.click('.datepicker:nth-child(18) > .datepicker-days > .table-condensed > tbody > tr:nth-child(1) > .day:nth-child(2)');
        const buttonApply = ('#contentbar #apply-payments-filter');
        await page.waitForSelector(buttonApply);
        await page.click(buttonApply);

        const elTablePayment = ('//th[contains(text(),\'Статус\')]');
        await page.waitForSelector(elTablePayment);
        await page.click(elTablePayment);
        await page.textContent(elTablePayment);

        expect(elTablePayment).to.have.string('Статус');
    });
  }
);

describe('Второй сьюит: навигация по меню', () => {
    let page;
    beforeEach(async () => {
        await run2();
        page = await goto2(url.idemobspb);
    });
    afterEach(async () => {
        await stop2();
    });


    it('Навигация по меню сайта: 1 вкладка Обзор', async () => {
        const usernameField = ('.container > #login-container > #login-form > .form-control:nth-child(2) > input');
        await page.waitForSelector(usernameField);
        await page.click(usernameField);
        await page.fill(usernameField, '');
        await page.fill(usernameField, 'demo');
        const passwordField = ('.container > #login-container > #login-form > .form-control:nth-child(3) > input');
        await page.waitForSelector(passwordField);
        await page.click(passwordField);
        await page.fill(passwordField, '');
        await page.fill(passwordField, 'demo');
        const loginButton = ('.container > #login-container > #login-form #login-button');
        await page.waitForSelector(loginButton);
        await page.click(loginButton);
        await page.waitForSelector('body #login-otp-button');
        await page.click('body #login-otp-button');

        const overview = ('.navbar #bank-overview');
        await page.waitForSelector(overview);
        await page.click(overview);
        const canSpend = await page.textContent('#contentbar > #header-container > .page-header > #can-spend > .text');

        expect(canSpend).to.have.string('Финансовая свобода');
    });

    it('Навигация по меню сайта: 2 вкладка Счета', async () => {
        const usernameField = ('.container > #login-container > #login-form > .form-control:nth-child(2) > input');
        await page.waitForSelector(usernameField);
        await page.click(usernameField);
        await page.fill(usernameField, '');
        await page.fill(usernameField, 'demo');
        const passwordField = ('.container > #login-container > #login-form > .form-control:nth-child(3) > input');
        await page.waitForSelector(passwordField);
        await page.click(passwordField);
        await page.fill(passwordField, '');
        await page.fill(passwordField, 'demo');
        const loginButton = ('.container > #login-container > #login-form #login-button');
        await page.waitForSelector(loginButton);
        await page.click(loginButton);
        await page.waitForSelector('body #login-otp-button');
        await page.click('body #login-otp-button');

        const accounts = ('.navbar #accounts > #accounts-index');
        await page.waitForSelector(accounts);
        await page.click(accounts);
        const accountHeader = await page.textContent('#contentbar > #accounts > thead > tr > .account');
        const buttonAccountNew = await page.textContent('#body > #contentbar > form > .form-actions > .btn');

        expect(accountHeader).to.have.string('Счёт');
        expect(buttonAccountNew).to.have.string('Открыть счёт');
    });

    it('Навигация по меню сайта: 3 вкладка Платежи и переводы', async () => {
        const usernameField = ('.container > #login-container > #login-form > .form-control:nth-child(2) > input');
        await page.waitForSelector(usernameField);
        await page.click(usernameField);
        await page.fill(usernameField, '');
        await page.fill(usernameField, 'demo');
        const passwordField = ('.container > #login-container > #login-form > .form-control:nth-child(3) > input');
        await page.waitForSelector(passwordField);
        await page.click(passwordField);
        await page.fill(passwordField, '');
        await page.fill(passwordField, 'demo');
        const loginButton = ('.container > #login-container > #login-form #login-button');
        await page.waitForSelector(loginButton);
        await page.click(loginButton);
        await page.waitForSelector('body #login-otp-button');
        await page.click('body #login-otp-button');

        const payments = ('.navbar #payments-form');
        await page.waitForSelector(payments);
        await page.click(payments);
        const history = ('#contentbar > #payments-left-menu > #dashboard-payment-type-menu > li > .history');
        await page.waitForSelector(history);
        await page.click(history);
        const historyHeader = await page.textContent('#contentbar > .tab-content > .page-header > h3 > small');

        expect(historyHeader).to.have.string('операций в системе');
    });

    it('Навигация по меню сайта: 4 вкладка Карты', async () => {
        const usernameField = ('.container > #login-container > #login-form > .form-control:nth-child(2) > input');
        await page.waitForSelector(usernameField);
        await page.click(usernameField);
        await page.fill(usernameField, '');
        await page.fill(usernameField, 'demo');
        const passwordField = ('.container > #login-container > #login-form > .form-control:nth-child(3) > input');
        await page.waitForSelector(passwordField);
        await page.click(passwordField);
        await page.fill(passwordField, '');
        await page.fill(passwordField, 'demo');
        const loginButton = ('.container > #login-container > #login-form #login-button');
        await page.waitForSelector(loginButton);
        await page.click(loginButton);
        await page.waitForSelector('body #login-otp-button');
        await page.click('body #login-otp-button');

        const cards = ('.navbar #cards-overview-index');
        await page.waitForSelector(cards);
        await page.click(cards);
        const cardsHeader = await page.textContent('#contentbar > .page-header > h1');

        expect(cardsHeader).to.have.string('Обзор карт');
    });

    it('Навигация по меню сайта: 5 вкладка Вклады', async () => {
        const usernameField = ('.container > #login-container > #login-form > .form-control:nth-child(2) > input');
        await page.waitForSelector(usernameField);
        await page.click(usernameField);
        await page.fill(usernameField, '');
        await page.fill(usernameField, 'demo');
        const passwordField = ('.container > #login-container > #login-form > .form-control:nth-child(3) > input');
        await page.waitForSelector(passwordField);
        await page.click(passwordField);
        await page.fill(passwordField, '');
        await page.fill(passwordField, 'demo');
        const loginButton = ('.container > #login-container > #login-form #login-button');
        await page.waitForSelector(loginButton);
        await page.click(loginButton);
        await page.waitForSelector('body #login-otp-button');
        await page.click('body #login-otp-button');

        const deposits = ('.navbar #deposits-index');
        await page.waitForSelector(deposits);
        await page.click(deposits);
        const buttonOpenDeposit = await page.textContent('#inner-wrapper > #body > #contentbar #btn-show-rates');

        expect(buttonOpenDeposit).to.have.string('Открыть вклад');
    });

    it('Навигация по меню сайта: 6 вкладка Кредиты', async () => {
        const usernameField = ('.container > #login-container > #login-form > .form-control:nth-child(2) > input');
        await page.waitForSelector(usernameField);
        await page.click(usernameField);
        await page.fill(usernameField, '');
        await page.fill(usernameField, 'demo');
        const passwordField = ('.container > #login-container > #login-form > .form-control:nth-child(3) > input');
        await page.waitForSelector(passwordField);
        await page.click(passwordField);
        await page.fill(passwordField, '');
        await page.fill(passwordField, 'demo');
        const loginButton = ('.container > #login-container > #login-form #login-button');
        await page.waitForSelector(loginButton);
        await page.click(loginButton);
        await page.waitForSelector('body #login-otp-button');
        await page.click('body #login-otp-button');

        const loans = ('.navbar #loans-index');
        await page.waitForSelector(loans);
        await page.click(loans);
        const buttonNewLoans = await page.textContent('#inner-wrapper > #body > #contentbar #loan-application-btn');

        expect(buttonNewLoans).to.have.string('Ознакомиться с предложениями');
    });

    it('Навигация по меню сайта: 7 вкладка Валюта', async () => {
        const usernameField = ('.container > #login-container > #login-form > .form-control:nth-child(2) > input');
        await page.waitForSelector(usernameField);
        await page.click(usernameField);
        await page.fill(usernameField, '');
        await page.fill(usernameField, 'demo');
        const passwordField = ('.container > #login-container > #login-form > .form-control:nth-child(3) > input');
        await page.waitForSelector(passwordField);
        await page.click(passwordField);
        await page.fill(passwordField, '');
        await page.fill(passwordField, 'demo');
        const loginButton = ('.container > #login-container > #login-form #login-button');
        await page.waitForSelector(loginButton);
        await page.click(loginButton);
        await page.waitForSelector('body #login-otp-button');
        await page.click('body #login-otp-button');

        const currency = ('.navbar #externaltraderoom-index');
        await page.waitForSelector(currency);
        await page.click(currency);
        const currencyHeader = await page.textContent('#inner-wrapper > #body > #contentbar > .page-header > h1');

        expect(currencyHeader).to.have.string('Обмен валюты');
    });

    it('Навигация по меню сайта: 8 вкладка Страхование', async () => {
        const usernameField = ('.container > #login-container > #login-form > .form-control:nth-child(2) > input');
        await page.waitForSelector(usernameField);
        await page.click(usernameField);
        await page.fill(usernameField, '');
        await page.fill(usernameField, 'demo');
        const passwordField = ('.container > #login-container > #login-form > .form-control:nth-child(3) > input');
        await page.waitForSelector(passwordField);
        await page.click(passwordField);
        await page.fill(passwordField, '');
        await page.fill(passwordField, 'demo');
        const loginButton = ('.container > #login-container > #login-form #login-button');
        await page.waitForSelector(loginButton);
        await page.click(loginButton);
        await page.waitForSelector('body #login-otp-button');
        await page.click('body #login-otp-button');

        const insurance = ('.navbar #insurance');
        await page.waitForSelector(insurance);
        await page.click(insurance);
        const insuranceHeader = await page.textContent('#inner-wrapper > #body > #contentbar > .page-header > h1');

        expect(insuranceHeader).to.have.string('Страхование');
    });
  }
);
